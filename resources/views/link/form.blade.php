@extends('layouts.app')

@section('title', 'Create short link')

@section('content')
<div class="row justify-content-md-center">
    <div class="col-md-3 py-5 my-5 mx-6">
        <h4 class="mb-3">Create short link</h4>
        <div class="alert alert-danger hidden" id="error-alert" role="alert"></div>
        <div class="alert alert-success hidden" id="success-alert" role="alert">
            Your link is <a id="success-link"></a>
        </div>

        <form class="" action="{{ route('link.store') }}" id="link-form" method="POST">
            <div class="input-group py-2">
                  <label for="url" class="form-label">URL</label>
                  <div class="input-group mb-3">
                    <input type="text" name="url" required id="url" class="form-control" placeholder="https://google.com">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button" id="form_btn">Create</button>
                    </div>
                  </div>
            </div>
            @csrf
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
    $('#form_btn').click(function() {
        $('#error-alert').html('').hide();
        $('#success-alert').hide();

        var data = $('#link-form').serialize();

        $.ajax({
            type: 'POST',
            url: '{{ route('link.store') }}',
            data: data,
            dataType: 'json'
        }).done(function(response) {
            $('#success-link').attr({href: response.link}).html(response.link);
            $('#success-alert').show();
        }).fail(function(response) {
            var errors = response.responseJSON.errors;

            for(var error in errors) {
                $('#error-alert').append('<p>' + errors[error] + '</p>');
            }

            $('#error-alert').show();
        });
    });
});
</script>
@endsection
