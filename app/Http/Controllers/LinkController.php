<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use App\Http\Requests\LinkRequest;

class LinkController extends Controller
{
    /**
     * Форма создания короткой ссылки
     */
    public function create() {
        return view('link.form');
    }

    /**
     * Обработка формы
     */
    public function store(LinkRequest $request) {
        $link = new Link();
        $link = $link->createShort($request->url);

        return json_encode(['link' => $link]);
    }

    /**
     * Редирект по короткой ссылке
     */
    public function redirect(Link $link) {
        return redirect($link->url);
    }
}
