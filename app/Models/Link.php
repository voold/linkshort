<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    /**
     * Создает короткую ссылку
     * @param string $url
     * @return string
     */
    public function createShort(string $url): string
    {
        // если уже существует короткая ссылка на этот url, возвращаем её (настройка зависит от config link.use.old_if_exists)
        if(config('link.use_old_if_link_exists') and $link = $this->checkExistShortLink($url)) {
            return $link;
        }

        $this->url = $url;
        $this->hash = $this->generateHash();
        $this->save();

        $link = $this->getLinkByHash($this->hash);
        return $link;
    }

    /**
     * Генерирует хэш для ссылки
     * @return string
     */
    private function generateHash(): string
    {
        $hash = md5(microtime() . date('ymdhis'));
        $hash = substr($hash, 0, config('link.length'));
        return $hash;
    }

    /**
     * Проверяем существование короткий ссылки для url, если существует, возвращает короткую ссылку
     * @param string $url
     * @return bool|string
     */
    private function checkExistShortLink(string $url): string
    {
        if($link = Link::where('url', $url)->first()) {
            return $this->getLinkByHash($link->hash);
        }

        return false;
    }

    /**
     * Возвращает полную короткую ссылку по хэшу
    */
    private function getLinkByHash(string $hash): string
    {
        return route('link.redirect', ['link' => $hash]);
    }
}
