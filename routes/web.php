<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LinkController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'link.'], function() {
    Route::get('/', [LinkController::class, 'create'])->name('create');
    Route::post('/', [LinkController::class, 'store'])->name('store');

    Route::get('/' . config('link.path') . '/{link:hash}', [LinkController::class, 'redirect'])->name('redirect');
});
