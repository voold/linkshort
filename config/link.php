<?php

return [
    'length' => 8, // длина хэша в ссылке / требует migrate:refresh при изменении в большую сторону
    'path' => 's', // https://domain/PATH/hash
    'use_old_if_link_exists' => true, // если ссылка уже существует, новая генерироваться не будет
];
